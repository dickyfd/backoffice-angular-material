import {
  AngularFireModule,
  FIREBASE_APP_NAME,
  FIREBASE_OPTIONS,
  FirebaseApp,
  ɵapplyMixins,
  ɵcacheInstance,
  ɵfirebaseAppFactory,
  ɵlazySDKProxy
} from "./chunk-LPDZSP6O.js";
import "./chunk-LVKJX3OM.js";
import "./chunk-XQINQ6G6.js";
import "./chunk-AR2SKMNP.js";
export {
  AngularFireModule,
  FIREBASE_APP_NAME,
  FIREBASE_OPTIONS,
  FirebaseApp,
  ɵapplyMixins,
  ɵcacheInstance,
  ɵfirebaseAppFactory,
  ɵlazySDKProxy
};
//# sourceMappingURL=@angular_fire_compat.js.map

import {
  MAT_BUTTON_CONFIG,
  MAT_FAB_DEFAULT_OPTIONS,
  MAT_FAB_DEFAULT_OPTIONS_FACTORY,
  MatAnchor,
  MatButton,
  MatButtonModule,
  MatFabAnchor,
  MatFabButton,
  MatIconAnchor,
  MatIconButton,
  MatMiniFabAnchor,
  MatMiniFabButton
} from "./chunk-AE22D2PT.js";
import "./chunk-DGWMOYGC.js";
import "./chunk-7MHTEROA.js";
import "./chunk-XQINQ6G6.js";
import "./chunk-AR2SKMNP.js";
export {
  MAT_BUTTON_CONFIG,
  MAT_FAB_DEFAULT_OPTIONS,
  MAT_FAB_DEFAULT_OPTIONS_FACTORY,
  MatAnchor,
  MatButton,
  MatButtonModule,
  MatFabAnchor,
  MatFabButton,
  MatIconAnchor,
  MatIconButton,
  MatMiniFabAnchor,
  MatMiniFabButton
};
//# sourceMappingURL=@angular_material_button.js.map

import {
  AngularFireAuth,
  AngularFireAuthModule,
  LANGUAGE_CODE,
  PERSISTENCE,
  SETTINGS,
  TENANT_ID,
  USE_DEVICE_LANGUAGE,
  USE_EMULATOR,
  ɵauthFactory
} from "./chunk-S3R5WPXE.js";
import "./chunk-ZVPG5RDO.js";
import "./chunk-HUJU2K6X.js";
import "./chunk-LPDZSP6O.js";
import "./chunk-LVKJX3OM.js";
import "./chunk-7MHTEROA.js";
import "./chunk-XQINQ6G6.js";
import "./chunk-AR2SKMNP.js";
export {
  AngularFireAuth,
  AngularFireAuthModule,
  LANGUAGE_CODE,
  PERSISTENCE,
  SETTINGS,
  TENANT_ID,
  USE_DEVICE_LANGUAGE,
  USE_EMULATOR,
  ɵauthFactory
};
//# sourceMappingURL=@angular_fire_compat_auth.js.map

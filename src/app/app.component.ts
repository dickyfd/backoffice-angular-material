import { AfterViewInit, Component, ViewChild } from '@angular/core';
import {
  MatDialog,
  MAT_DIALOG_DATA,
  MatDialogRef,
  MatDialogTitle,
  MatDialogContent,
  MatDialogActions,
  MatDialogClose,
} from '@angular/material/dialog';
import {MatSort, MatSortModule, SortDirection} from '@angular/material/sort';
import { AddMemberComponent } from './add-member/add-member.component';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { HttpService } from './service/http.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})


export class AppComponent implements AfterViewInit {
  title = 'backoffice';
  selected = '';

  animal: string = '';
  name: string = '';
  dataChemical = [];
  Member:any
  statusChange:any = {
    status: "",
    name: ""
  }

  displayedColumns: string[] = ['name', 'weight', 'height', 'key'];
  dataSource:any;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;

  constructor(
    public dialog: MatDialog,
    private httpService: HttpService,
    public toastr: ToastrService
  ) {}

  ngAfterViewInit() {
    let s = this.httpService.GetMembersList(); 
    s.snapshotChanges().subscribe(data => {
      console.log(data)
      this.Member = [];
      data.forEach(item => {
        let a = item.payload.toJSON(); 
        a['key'] = item.key;
        this.Member.push(a as any);
      })
      console.log(this.Member)
      const reversed = this.Member.reverse();
      console.log('reversed:', reversed);
      this.dataSource = new MatTableDataSource<any>(reversed)
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
      console.log("statuschange")
      console.log(this.statusChange.status)
    })
    
  }

  ngOnInit() {
    
  }

  delete(id:any, name:any) {
    this.httpService.DeleteMember(id)
    this.toastr.error(
      name + ' successfully Deleted!'
    );
  }

  openDialog(status:any, id:any): void {
    const dialogRef = this.dialog.open(AddMemberComponent, {
      data: {name: this.name, status: status, weight: "", height: "", key: id},
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.statusChange = result
      console.log(this.statusChange)
      if (this.statusChange.status == "Add") {
        console.log('success add')
        this.toastr.success(
          this.statusChange.name + ' successfully added!'
        );
      } else if (this.statusChange.status == "Edit") {
        this.toastr.info(
          this.statusChange.name + ' successfully Updated!'
        );
      } else if (this.statusChange.status == "Delete") {

      }
    });
  }
}
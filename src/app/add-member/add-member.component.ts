import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { AppComponent } from '../app.component';
import { HttpService } from '../service/http.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-member',
  templateUrl: './add-member.component.html',
  styleUrl: './add-member.component.css'
})
export class AddMemberComponent {
  selected = '';
  name = '';
  weight = '';
  height = '';

  constructor(
    public dialogRef: MatDialogRef<AddMemberComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private httpService: HttpService,
    public toastr: ToastrService
  ) {}

  onNoClick(): void {
    this.dialogRef.close();
  }

  ngOnInit() {
    console.log(this.data)
    if (this.data.status == 'Edit') {
      this.httpService
      .GetMember(this.data.key)
      .valueChanges()
      .subscribe((data) => {
        console.log(data)
        this.name = data.name
        this.weight = data.weight
        this.height = data.height
      });
    }
  }

  save() {
    let addData = {
      name: this.name,
      weight: this.weight,
      height: this.height,
    }
    this.data.name = this.name
    console.log(addData)
    if (this.data.status == 'Edit') {
      this.httpService.UpdateMember(addData, this.data.key)
    } else {
      this.httpService.AddMember(addData)
    }
  }
  reset() {
    
  }
}

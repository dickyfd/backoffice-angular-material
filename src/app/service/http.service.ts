import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {
  AngularFireDatabase,
  AngularFireList,
  AngularFireObject,
} from '@angular/fire/compat/database';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  membersRef: AngularFireList<any>;
  memberRef: AngularFireObject<any>;

  private baseURL = `http://localhost:3000`

  constructor(
    private http: HttpClient,
    private db: AngularFireDatabase
    ) { }

  AddMember(member: any) {
    this.membersRef = this.db.list('/members');
    this.membersRef.push(member)
  }

  GetMember(id: string) {
    this.memberRef = this.db.object('/members/' + id);
    return this.memberRef;
  }

  GetMembersList() {
    this.membersRef = this.db.list('/members');
    return this.membersRef;
  }

  UpdateMember(member: any, id:any) {
    this.memberRef = this.db.object('/members/' + id);
    this.memberRef.update(member)
  }

  DeleteMember(id: string) {
    this.memberRef = this.db.object('/members/' + id);
    try {
      this.memberRef.remove();
    } catch (error) {
      
    }
  }

  getAllData(): Observable<any> {
    return this.http.get(`${this.baseURL}/datas`)
 }
}
